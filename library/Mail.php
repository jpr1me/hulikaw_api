<?php

use Phalcon\Mvc\User\Component,
	Phalcon\Mvc\View;

require_once __DIR__ . '/Swift/swift_required.php';
/**
 *
 * Sends e-mails based on pre-defined templates
 */
class Mail extends Component
{


	public function email(){
		echo 'email';
	}

	protected $_transport;

	/**
	 * Applies a template to be used in the e-mail
	 *
	 * @param string $name
	 * @param array $params
	 */
	public function getTemplate($name, $params){

		$parameters = array_merge(array(
			'publicUrl' => $this->config->application->baseUri,
		), $params);

		return $this->view->getRender('emailTemplates', $name, $parameters, function($view){
			$view->setRenderLevel(View::LEVEL_LAYOUT);
		});
		return $view->getContent();
	}

	/**
	 * Sends e-mails via gmail based on predefined templates
	 *
	 * @param array $to
	 * @param string $subject
	 * @param string $name
	 * @param array $params
	 */
	public function send($to, $subject, $name, $params)
	{

		$senderName  = 'Hulikaw';
		$senderEmail = 'noreply@hulikaw.com';

		$server 	= 'smtp.gmail.com';
		$port 		= 465;
		$security 	= 'ssl';
		$username 	= 'david.kernelx@gmail.com';
		$password 	= '<Pr0gr@mm3rx>';

		//Settings
		$mailSettings = $this->mail;
		//$template = $this->getTemplate($name, $params);
		// Create the message
		$message = Swift_Message::newInstance()
  			->setSubject($subject)
  			->setTo($to)
  			->setFrom(array(
  				$senderEmail => $senderName
  			))
  			->setBody( 'HuliKaw! has sent you a new report.  Please check your email or login your dashboard.', 'text/html');
  			if (!$this->_transport) {
				$this->_transport = Swift_SmtpTransport::newInstance(
					$server,
					$port,
					$security
				)
		  			->setUsername($username)
		  			->setPassword($password);
		  	}

		  	// Create the Mailer using your created Transport
			$mailer = Swift_Mailer::newInstance($this->_transport);

			return $mailer->send($message);
	}

}