<?php

use Phalcon\Mvc\Model,
    Phalcon\Mvc\Model\Message,
    Phalcon\Mvc\Model\Validator\InclusionIn,
    Phalcon\Mvc\Model\Validator\Uniqueness;

class ReportEvidenceFiles extends Model {

	public $id;

    public $user_id;
    public $report_id;

	public function initialize()
    {
        $this->belongsTo("user_id", "Users", "id");
        $this->belongsTo("report_id", "Reports", "id");
    }

}




?>