<?php

use Phalcon\Mvc\Model;

class TblReportStatusThreads extends Model {

	/**
	 * [$id description]
	 * @var [type]
	 */
	public $id;

	/**
	 * [$pid description]
	 * @var [type]
	 */
	public $pid;

	/**
	 * [$report_status_id description]
	 * @var [type]
	 */
	public $report_status_id;

	/**
	 * [$user_id description]
	 * @var [type]
	 */
	public $user_id;

	/**
	 * [$poster description]
	 * @var [type]
	 */
	public $poster;

	/**
	 * [$title description]
	 * @var [type]
	 */
	public $title;

	/**
	 * [$body description]
	 * @var [type]
	 */
	public $body;

	/**
	 * [$created_on description]
	 * @var [type]
	 */
	public $created_on;

	/**
	 * [$modified description]
	 * @var [type]
	 */
	public $modified;


	public function initialize(){
        $this->setConnectionService('db2');
	}

    public function getSource(){
        return "report_status_threads";
    }

}