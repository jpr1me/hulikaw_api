<?php

use Phalcon\Mvc\Model,
    Phalcon\Mvc\Model\Message,
    Phalcon\Mvc\Model\Validator\InclusionIn,
    Phalcon\Mvc\Model\Validator\Uniqueness;
    
class TblPosts extends \Phalcon\Mvc\Model {
	
	public $id;

    public $user_id;

    public $government_agency_id;
    public $category_id;

	public function initialize()
    {   
        $this->setConnectionService('db2');
        $this->belongsTo("user_id", "Users", "id");
        $this->belongsTo("government_agency_id", "GovernmentAgencies", "id");
        $this->belongsTo("category_id", "Categories", "id");
        $this->hasMany("id", "Comments", "post_id");
        $this->hasMany("id", "PostMoods", "post_id");
        $this->hasMany("id", "PostFiles", "post_id");
    }
	
    
}