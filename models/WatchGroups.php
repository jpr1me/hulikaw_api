<?php

use Phalcon\Mvc\Model,
    Phalcon\Mvc\Model\Message,
    Phalcon\Mvc\Model\Validator\InclusionIn,
    Phalcon\Mvc\Model\Validator\Uniqueness;

class WatchGroups extends Model {

	public $id; // AUTO_INCREMENT, primary
	public $created_on;  
    public $unique_id; // #jejomarbinay
    public $user_id; // founder
    public $display_name; 
    public $visibility;
    public $members;

	public function initialize(){
        $this->setConnectionService('db2');
    }
    
    public function getSource(){
        return "watch_groups";
    }

}

?>