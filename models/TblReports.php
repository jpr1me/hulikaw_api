<?php

use Phalcon\Mvc\Model,
    Phalcon\Mvc\Model\Message,
    Phalcon\Mvc\Model\Validator\InclusionIn,
    Phalcon\Mvc\Model\Validator\Uniqueness;

class TblReports extends Model {

	public $id;
	public $created_on;
	public $modified;
	public $user_id;
	public $report_code;
	public $category_id;
	public $first_name;
	public $middle_name;
	public $last_name;
	public $suffix;
	public $govt_agency_id; // null
	public $position_rank; // null
	public $asset_id; // null
	public $report_status_id; // null
	public $description; 
	public $other_info;
	public $agency_name;
	public $city_name;
	public $province_name;
	public $district_id;
	public $watch_keyword;
	public $img_date;
	public $img_location;

	public function initialize()
    {
        $this->setConnectionService('db2');
    }

    public function getSource()
    {
        return "reports";
    }

}



?>