<?php

use Phalcon\Mvc\Model;


class TblConversations extends Model {


	/**
	 * [$id description]
	 * @var [type]
	 */
	public $id;

	/**
	 * [$created_on description]
	 * @var [type]
	 */
	public $created_on;

	/**
	 * [$status description]
	 * @var [type]
	 */
	public $status;


    public function getSource(){
        return "conversations";
    }


	public function initialize(){

		$this->setConnectionService('db2');

        $this->hasMany(
            'id',
            'ConversationReplies',
            'conversation_id',
            array(
                'alias'    => 'replies'
            )
        );

        $this->hasMany(
            'id',
            'ConversationUserKeys',
            'conversation_id',
            array(
                'alias'    => 'conversationuserkeys'
            )
        );

     
	}





}