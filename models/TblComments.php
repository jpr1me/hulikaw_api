<?php

use Phalcon\Mvc\Model,
    Phalcon\Mvc\Model\Message,
    Phalcon\Mvc\Model\Validator\InclusionIn,
    Phalcon\Mvc\Model\Validator\Uniqueness;

class TblComments extends Model {

	public $id;
	public $created_on;
	public $report_code;
	public $user_id;
	public $content;

	public function initialize()
    {
        $this->setConnectionService('db2');
    }

    public function getSource()
    {
        return "comments";
    }

}



?>