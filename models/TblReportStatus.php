<?php

use Phalcon\Mvc\Model;

class TblReportStatus extends Model {

	/**
	 * [$id description]
	 * @var [type]
	 */
	public $id;

	/**
	 * [$created_on description]
	 * @var [type]
	 */
	public $created_on;

	/**
	 * [$modified description]
	 * @var [type]
	 */
	public $modified;

	/**
	 * [$report_status_category_id description]
	 * @var [type]
	 */
	public $report_status_stage_id;

	/**
	 * [$report_id description]
	 * @var [type]
	 */
	public $report_id;

	/**
	 * [$user_id description]
	 * @var [type]
	 */
	public $user_id;

	/**
	 * [$agency_type_id description]
	 * @var [type]
	 */
	public $agency_type_id;


	/**
	 * [initialize description]
	 * @return [type] [description]
	 */
	public function initialize(){
        $this->setConnectionService('db2');
    }
    public function getSource(){
        return "report_status";
    }







}