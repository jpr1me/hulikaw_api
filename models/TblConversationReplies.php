<?php

use Phalcon\Mvc\Model,
    Phalcon\Mvc\Model\Message,
    Phalcon\Mvc\Model\Validator\InclusionIn,
    Phalcon\Mvc\Model\Validator\Uniqueness;

class TblConversationReplies extends Model {


    /**
     * [$id description]
     * @var [type]
     */
    public $id;

    /**
     * [$time description]
     * @var [type]
     */
    public $time;

    /**
     * [$reply description]
     * @var [type]
     */
    public $reply;

    /**
     * [$user_id description]
     * @var [type]
     */
    public $user_id;

    /**
     * [$conversation_id description]
     * @var [type]
     */
    public $conversation_id;

    /**
     * [$ip_address description]
     * @var [type]
     */
    public $ip_address;

    /**
     * [$status description]
     * @var [type]
     */
    public $status;


	public function initialize(){

        $this->setConnectionService('db2');


        $this->belongsTo(
            'conversation_id', 'Conversations', 'id',
            array(
                'alias' => 'conversations'
            )
        );

        $this->belongsTo(
            'user_id', 'AgencyAccounts', 'id',
            array(
                'alias' => 'agencyaccounts'
            )
        );

        $this->belongsTo(
            'user_id', 'Users', 'id',
            array(
                'alias' => 'users'
            )
        );



    }

    public function getSource()
    {
        return "conversation_replies";
    }

}

