<?php

use Phalcon\Mvc\Model,
    Phalcon\Mvc\Model\Message,
    Phalcon\Mvc\Model\Validator\InclusionIn,
    Phalcon\Mvc\Model\Validator\Uniqueness;

class AndroidDeviceTokens extends Model {

	public $id;
	public $user_id;
    public $token_id;
    public $status;

	public function initialize()
    {
        $this->setConnectionService('db2');
    }

    public function getSource()
    {
        return "user_device_tokens";
    }

}



?>