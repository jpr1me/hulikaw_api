<?php


use Phalcon\Mvc\Model;


class TblNotifications extends Model {

	/**
	 * [$id description]
	 * @var [type]
	 */
	public $id;

	/**
	 * [$created_on description]
	 * @var [type]
	 */
	public $created_on;

	/**
	 * [$modified description]
	 * @var [type]
	 */
	public $modified;

	/**
	 * [$user_id description]
	 * @var [type]
	 */
	public $user_id;

	/**
	 * [$recipient_id description]
	 * @var [type]
	 */
	public $recipient_id;

	/**
	 * [$message description]
	 * @var [type]
	 */
	public $message;

	/**
	 * [$was_read description]
	 * @var [type]
	 */
	public $was_read;

	/**
	 * [$type description]
	 * @var [type]
	 */
	public $type;

	/**
	 * [$account_type description]
	 * @var [type]
	 */
	public $account_type;
	public $delivered;
	public $title;
    /**
     * [initialize description]
     * @return [type] [description]
     */
    public function initialize(){
        $this->setConnectionService('db2');
    }

    /**
     * [getSource description]
     * @return [type] [description]
     */
    public function getSource(){
        return "notifications";
    }

}