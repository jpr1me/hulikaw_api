<?php

use Phalcon\Mvc\Model,
    Phalcon\Mvc\Model\Message,
    Phalcon\Mvc\Model\Validator\InclusionIn,
    Phalcon\Mvc\Model\Validator\Uniqueness;

class WatchGroupMembers extends Model {

	public $id; // AUTO_INCREMENT, primary
	public $created_on;
    public $watch_group_id; // watch_groups' unique_id
    public $user_id; // poster
    public $report_id; // id of report
    public $subscribe; 

	public function initialize()
    {
        $this->setConnectionService('db2');
    }
    
    public function getSource()
    {
        return "watch_group_members";
    }

}

?>

