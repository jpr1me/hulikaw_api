<?php

use Phalcon\Mvc\Model,
    Phalcon\Mvc\Model\Message,
    Phalcon\Mvc\Model\Validator\InclusionIn,
    Phalcon\Mvc\Model\Validator\Uniqueness;

class TblAgenciesDof extends Model {

	public $id;
	public $abbreviation; 
    public $agency_name; // longname

	public function initialize()
    {
        $this->setConnectionService('db2');
    }

    public function getSource()
    {
        return "govt_agencies";
    }

}



?>