<?php

use Phalcon\Mvc\Model,
    Phalcon\Mvc\Model\Message,
    Phalcon\Mvc\Model\Validator\InclusionIn,
    Phalcon\Mvc\Model\Validator\Uniqueness;

class TblUsers extends Model {

	public $id; // AUTO_INCREMENT, primary
	public $created_on;  
    public $codename; 
    public $email;
    public $password;
    public $role_id;
    public $banned;
    public $suspended;
    public $active;
    public $token;
    public $avatar_id;

	public function initialize()
    {
        $this->setConnectionService('db2');
    }
    

    public function getSource()
    {
        return "users";
    }

}

?>