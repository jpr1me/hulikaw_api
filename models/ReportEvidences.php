<?php

use Phalcon\Mvc\Model,
    Phalcon\Mvc\Model\Message,
    Phalcon\Mvc\Model\Validator\InclusionIn,
    Phalcon\Mvc\Model\Validator\Uniqueness;

class ReportEvidences extends Model {

	public $id; // AUTO_INCREMENT, primary
	public $created_on; // null
	public $modified; // null
	public $user_id; // not null
	public $report_id; // not null
	public $filename; // null

	public function initialize()
    {
        $this->setConnectionService('db2');
    }
    

    public function getSource()
    {
        return "report_evidences";
    }

}

?>