<?php

use Phalcon\Mvc\Model,
    Phalcon\Mvc\Model\Message,
    Phalcon\Mvc\Model\Validator\InclusionIn,
    Phalcon\Mvc\Model\Validator\Uniqueness;

class TblRates extends Model {

	public $id;
	public $created;
	public $report_code;
	public $user_id;
	public $rate;

	public function initialize()
    {
        $this->setConnectionService('db2');
    }

    public function getSource()
    {
        return "rates";
    }

}



?>