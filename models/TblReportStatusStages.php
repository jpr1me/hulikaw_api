<?php

use Phalcon\Mvc\Model;

class TblReportStatusStages extends Model {


    /**
     * [$id description]
     * @var [type]
     */
    public $id;

    /**
     * [$stage description]
     * @var [type]
     */
    public $stage;

    /**
     * [$description description]
     * @var [type]
     */
    public $description;

    /**
     * [$percentage description]
     * @var [type]
     */
    public $percentage;


    /**
     * [$agency_type_id description]
     * @var [type]
     */
    public $agency_type_id;

    /**
     * [initialize description]
     * @return [type] [description]
     */
    public function initialize(){
        $this->setConnectionService('db2');
    }

    /**
     * [getSource description]
     * @return [type] [description]
     */
    public function getSource(){
        return "report_status_stages";
    }






}