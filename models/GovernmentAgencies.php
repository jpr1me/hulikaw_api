<?php

use Phalcon\Mvc\Model,
    Phalcon\Mvc\Model\Message,
    Phalcon\Mvc\Model\Validator\InclusionIn,
    Phalcon\Mvc\Model\Validator\Uniqueness;

class GovernmentAgencies extends Model {

	public $id;

    public $abbreviation;

	public function initialize()
    {
        $this->hasMany("id", "Reports", "government_agency_id");
    }

}




?>