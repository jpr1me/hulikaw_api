<?php

use Phalcon\Mvc\Model,
    Phalcon\Mvc\Model\Message,
    Phalcon\Mvc\Model\Validator\InclusionIn,
    Phalcon\Mvc\Model\Validator\Uniqueness;

class TblConversationUserKeys extends Model {

    /**
     * [$id description]
     * @var [type]
     */
    public $id;

    /**
     * [$host_id description]
     * @var [type]
     */
    public $host_id;

    /**
     * [$sub_id description]
     * @var [type]
     */
    public $sub_id;

    /**
     * [$conversation_id description]
     * @var [type]
     */
    public $conversation_id;

    /**
     * [$account_type_host description]
     * @var [type]
     */
    public $account_type_host;

    /**
     * [$account_type_sub description]
     * @var [type]
     */
    public $account_type_sub;

    /**
     * [$chat_type description]
     * @var [type]
     */
    public $chat_type;


    public function getSource(){
        return "conversation_user_keys";
    }

	public function initialize(){

        $this->setConnectionService('db2');

        $this->belongsTo(
            'conversation_id','TblConversations','id',
            array(
                'alias' => 'Conversations'
            )
        );

        $this->belongsTo(
            'host','TblAgencyAccounts','id',
            array(
                'alias' => 'AgencyAccountsHost'
            )
        );

        $this->belongsTo(
            'host','Users','id',
            array(
                'alias' => 'UserHost'
            )
        );

        $this->belongsTo(
            'sub', 'TblAgencyAccounts', 'id',
            array(
                'alias' => 'AgencyAccountsSub'
            )
        );

        $this->belongsTo(
            'sub', 'TblUsers', 'id',
            array(
                'alias' => 'UserSub'
            )
        );


    }


}

