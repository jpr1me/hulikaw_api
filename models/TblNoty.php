<?php

use Phalcon\Mvc\Model,
    Phalcon\Mvc\Model\Message,
    Phalcon\Mvc\Model\Validator\InclusionIn,
    Phalcon\Mvc\Model\Validator\Uniqueness;

class TblNoty extends Model {

	public $id;
	public $app_id;
	public $user_id;

	public function initialize()
    {
        $this->setConnectionService('db2');
    }

    public function getSource()
    {
        return "noty";
    }

}



?>