<?php


use Phalcon\Mvc\Model;

class TblAgencyAccounts extends Model {

	public $id;
	public $created_on;
	public $modified;
	public $codename;
	public $password;
	public $email;
	public $mobile;
	public $first_name;
	public $middle_name;
	public $last_name;
	public $suffix;
	public $designation;
	public $govt_agency_id;
	public $r_m_r;
	public $agency_type_id;
	public $agency_division_id;
	public $change_password;
	public $banned;
	public $suspended;
	public $active;
    public $role_id;
    public $division_level;
    public $token;



    /**
     * [initialize description]
     * @return [type] [description]
     */
    public function initialize(){
        $this->setConnectionService('db2');
    }


	/**
	 * [initialize description]
	 * @return [type] [description]
	 */
	public function getSource(){

        return "agency_accounts";

	}


}

