<?php

use Phalcon\Mvc\Model,
    Phalcon\Mvc\Model\Message,
    Phalcon\Mvc\Model\Validator\InclusionIn,
    Phalcon\Mvc\Model\Validator\Uniqueness;

class PostFiles extends \Phalcon\Mvc\Model {
	
	public $id;

    public $user_id;

    public $post_id;

	public function initialize()
    {
        $this->belongsTo("user_id", "Users", "id");
        $this->belongsTo("post_id", "Posts", "id");
        
    }
	
    
}