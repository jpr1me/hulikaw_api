<?php
use Phalcon\Mvc\Model;


class AgencyTypeCategories extends Model {

	/**
	 * [$id description]
	 * @var [type]
	 */
	public $id;

	/**
	 * [$agency_type_id description]
	 * @var [type]
	 */
	public $agency_type_id;

	/**
	 * [$govt_agency_id description]
	 * @var [type]
	 */
	public $govt_agency_id;


	public function initialize(){
        $this->setConnectionService('db2');
    }

    public function getSource(){
        return "agency_type_categories";
    }


}